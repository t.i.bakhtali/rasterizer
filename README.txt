Hello and welcome to our 3D Engine.
This engine was made by Tariq Bakhtali and Merijn Schepers

Contact info:
Tariq Bakhtali, 5631394, t.i.bakhtali@students.uu.nl
Merijn Schepers, 6504477, m.schepers2@students.uu.nl

The features included in this engine are:
- Interactive camera: the camera can be moved and rotated
- A scene graph with unlimited depth
- Phong shading model implemented
- Multiple lights: the amount can be easily changed at run-time,
	and the lights can be moved realtime
- Normal mapping
- Skybox
- Vignetting and chromatic aberration
- Generic colorization with a color cube
- Box filter with variable kernel width
- HDR glow (or bloom)

The engine is operated in the following way:
- You can use the mouse to "drag" the scene around
- You can use the scroll wheel to move the camera closer or further away from the scene
- You can use the up and down arrows to rotate the scene around the y-axis
- (You can also use the left and right arrows to rotate around a different axis, but then the scene is not 2.5D anymore)
- You can use the buttons M and N to increase or decrease the blur

Adding and/or changing objects can be done in the AddObjects function in MyApplication.cs


ENJOY


######################################################################################
#                                                                                    # 
#                            ,.--------._                                            #
#                           /            ''.                                         #
#                         ,'                \     |"\                /\          /\  #
#                /"|     /                   \    |__"              ( \\        // ) #
#               "_"|    /           z#####z   \  //                  \ \\      // /  #
#                 \\  #####        ##------".  \//                    \_\\||||//_/   #
#                  \\/-----\     /          ".  \                      \/ _  _ \     #
#                   \|      \   |   ,,--..       \                    \/|(O)(O)|     #
#                   | ,.--._ \  (  | ##   \)      \                  \/ |      |     #
#                   |(  ##  )/   \ `-....-//       |///////////////_\/  \      /     #
#                     '--'."      \                \              //     |____|      #
#                  /'    /         ) --.            \            ||     /      \     #
#               ,..|     \.________/    `-..         \   \       \|     \ 0  0 /     #
#            _,##/ |   ,/   /   \           \         \   \       U    / \_//_/      #
#          :###.-  |  ,/   /     \        /' ""\      .\        (     /              #
#         /####|   |   (.___________,---',/    |       |\=._____|  |_/               #
#        /#####|   |     \__|__|__|__|_,/             |####\    |  ||                #
#       /######\   \      \__________/                /#####|   \  ||                #
#      /|#######`. `\                                /#######\   | ||                #
#     /++\#########\  \                      _,'    _/#########\ | ||                #
#    /++++|#########|  \      .---..       ,/      ,'##########.\|_||                #
#   //++++|#########\.  \.              ,-/      ,'########,+++++\\_\\               #
#  /++++++|##########\.   '._        _,/       ,'######,''++++++++\                  #
# |+++++++|###########|       -----."        _'#######' +++++++++++\                 #
# |+++++++|############\.     \\     //      /#######/++++++++++++++\                #
#      ________________________\\___//______________________________________         #
#     / ____________________________________________________________________)        #
#    / /              _                                             _                #
#    | |             | |                                           | |               #
#     \ \            | | _           ____           ____           | |  _            #
#      \ \           | || \         / ___)         / _  )          | | / )           #
#  _____) )          | | | |        | |           (  __ /          | |< (            #
# (______/           |_| |_|        |_|            \_____)         |_| \_)           #
#                                                                                    #
######################################################################################
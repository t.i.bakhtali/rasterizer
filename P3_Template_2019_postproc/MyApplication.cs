using System;
using System.Diagnostics;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Template
{
    class MyApplication
    {
        // member variables
        public Surface screen;                  // background surface for printing etc.
        const float PI = 3.1415926535f;         // PI
        Stopwatch timer;                        // timer for measuring frame duration
        RenderTarget scenePass;
        RenderTarget pass;                    // intermediate render target
        RenderTarget pass2;
        ScreenQuad quad;                        // screen filling quad for post processing
        bool useRenderTarget = true;
        Node skybox;

        World world;
        Camera camera;

        int kernelSize = 300;

        Texture colorcube;

        public static LightManager lightManager;
        public static ShaderManager shaderManager;
        float t = 0;
        
        // initialize
        public void Init()
        {
            lightManager = new LightManager();
            shaderManager = new ShaderManager();

            lightManager.AddLight(0, 20, -50, .5f, .5f, .5f, 1f);
            lightManager.AddLight(50, 20, 50, .5f, .5f, .5f, 3.5f);
            lightManager.AddLight(-50, 20, 50, 0, .5f, .5f, 4);
            lightManager.AddLight(0, 20, -50, .5f, .5f, 0, 2);
            // create shaders
            shaderManager.AddShader("texture", new Shader("TextureShader"));
            shaderManager.AddShader("cube", new Shader("CubemapShader"));
            shaderManager.AddShader("vignette", new Shader("PostProc", "PostProcVign"));
            shaderManager.AddShader("chromatic", new Shader("PostProc", "PostProcChrom"));
            shaderManager.AddShader("blur", new Shader("PostProcBlur", "PostProcBlur"));
            shaderManager.AddShader("bright", new Shader("PostProc", "PostProcBright"));
            shaderManager.AddShader("bloom", new Shader("PostProc", "PostProcBloom"));
            shaderManager.AddShader("color", new Shader("PostProc", "PostProcColC"));
            shaderManager.AddShader("skybox", new Shader("SkyboxShader"));

            Texture wood = new Texture("wood.jpg");
            Texture brick = new Texture("texture_brick1.jpg");
            colorcube = new Texture("colorcubenormal.png");

            Cubemap cubemap = new Cubemap("skybox1", ".jpg");
            
            skybox = new Node(new Mesh("cube"), "skybox", wood.id, brick);
            skybox.SetTNode(300f, Vector3.Zero, new Vector3(1, 0, 0), (float) -Math.PI / 2);
            skybox.Rotate(new Vector3(0, 1, 0), (float)Math.PI);
            skybox.Rotate(new Vector3(0, 0, 1), (float)-Math.PI / 5);

            world = new World();
            world.SetTNode(1, Vector3.Zero, new Vector3(0, 1, 0), 0);

            AddObjects();

            Matrix4 TView = Matrix4.CreatePerspectiveFieldOfView(1.2f, OpenTKApp.resolution.X / OpenTKApp.resolution.Y, .1f, 1000);
            camera = new Camera(new Vector3(0, 10, 0), 0, TView);
            
            // initialize stopwatch
            timer = new Stopwatch();
            timer.Reset();
            timer.Start();

            // create the render target
            scenePass = new RenderTarget(screen.width, screen.height);
            pass = new RenderTarget(screen.width, screen.height);
            pass2 = new RenderTarget(screen.width, screen.height);
            quad = new ScreenQuad();
        }

        void AddObjects()
        {
            Texture wood_c = new Texture("wood_color.jpg");
            Texture wood_n = new Texture("wood_norm.jpg");
            Texture wood_floor_c = new Texture("wood_floor_color.jpg");
            Texture wood_floor_n = new Texture("wood_floor_norm.jpg");
            Texture marble_c = new Texture("marble_color.jpg");
            Texture marble_n = new Texture("marble_norm.jpg");
            Texture metal_c = new Texture("metal_scale_color.jpg");
            Texture metal_n = new Texture("metal_scale_norm.jpg");

            Node floor = new Node(new Mesh("floor"), "cube", wood_floor_c.id, wood_floor_n);
            floor.SetTNode(5, Vector3.Zero, new Vector3(0, 1, 0), 0);

            Node anchor = new RotatingObject();
            anchor.SetTNode(1, new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

            Node table = new Node(new Mesh("table"), "cube", wood_c.id, wood_n);
            table.SetTNode(2, new Vector3(4, -1.9f, 4), new Vector3(0, 1, 0), 0);

            Node aquarium = new Node(new Mesh("aquarium"), "texture", marble_c.id, marble_n);
            aquarium.SetTNode(.5f, new Vector3(.35f, 1.43f, -.6f), new Vector3(0, 1, 0), 0);

            Node fishanchor = new RotatingObject(_speed: -.01f);
            fishanchor.SetTNode(.5f, new Vector3(-.7f, 0, 1.5f), new Vector3(.1f, 1, 0), 0);

            Node fish = new Node(new Mesh("fish1"), "texture", metal_c.id, metal_n);
            fish.SetTNode(.04f, new Vector3(-.3f, 1.5f, .7f), new Vector3(1, 0, 0), -1);

            Node fish2 = new Node(new Mesh("fish1"), "texture", metal_c.id, metal_n);
            fish2.SetTNode(.04f, new Vector3(.3f, 1.5f, -.7f), new Vector3(1, 0, 0), -1);
            fish2.Rotate(new Vector3(0, 1, 0), 2.5f);

            fishanchor.AddChild(fish);
            fishanchor.AddChild(fish2);
            aquarium.AddChild(fishanchor);
            table.AddChild(aquarium);
            anchor.AddChild(table);
            floor.AddChild(anchor);
            world.AddChild(floor);
        }

        // tick for background surface
        public void Tick()
        {
            t += 0.01f;
            camera.Update();
            float sin = (float) Math.Sin(t) * 50;
            lightManager.SetLight(0, 0, 30, sin);
        }

        // tick for OpenGL rendering code
        public void RenderGL()
        {
            // measure frame duration
            float frameDuration = timer.ElapsedMilliseconds;
            timer.Reset();
            timer.Start();

            Matrix4 transform = camera.GetTransform;
            
            if (useRenderTarget)
            {
                // enable render target
                scenePass.Bind();

                // render scene to render target
                skybox.Render(new Matrix4(camera.GetRotation) * camera.TView, Matrix4.Identity, frameDuration, camera.transform.Column3.Xyz);
                world.Render(transform, Matrix4.Identity,  frameDuration, camera.transform.Column3.Xyz);
                scenePass.Unbind();
                //postprocessing
                PostProcess();
            }
            else
            {
                // render scene directly to the screen
                world.Render(transform, Matrix4.Identity, frameDuration, camera.transform.Column3.Xyz);
            }
        }

        public void PostProcess()
        {
            pass2.Bind();
            quad.CorrectColors(shaderManager.GetShader("color"), scenePass.GetTextureID(), colorcube.id);
            pass2.Unbind();

            pass.Bind();
            quad.Render(shaderManager.GetShader("bright"), pass2.GetTextureID());
            pass.Unbind();

            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Key.M)) kernelSize -= 5;
            if (keyboard.IsKeyDown(Key.N)) kernelSize += 5;

            if (kernelSize < 100) kernelSize = 100;
            if (kernelSize > screen.width) kernelSize = 1000;

            pass2.Bind();
            quad.Blur(shaderManager.GetShader("blur"), pass.GetTextureID(), true, kernelSize);
            pass2.Unbind();
            pass.Bind();
            quad.Blur(shaderManager.GetShader("blur"), pass2.GetTextureID(), false, kernelSize);
            pass.Unbind();

            pass2.Bind();
            quad.Bloom(shaderManager.GetShader("bloom"), scenePass.GetTextureID(), pass.GetTextureID());
            pass2.Unbind();

            pass.Bind();
            quad.Render(shaderManager.GetShader("vignette"), pass2.GetTextureID());
            pass.Unbind();
            quad.Render(shaderManager.GetShader("chromatic"), pass.GetTextureID());

        }

    }
}
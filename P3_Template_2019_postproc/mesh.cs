﻿ using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Template
{
    // mesh and loader based on work by JTalton; http://www.opentk.com/node/642

    public class Mesh
	{
		// data members
		public ObjVertex[] vertices;            // vertex positions, model space
		public ObjTriangle[] triangles;         // triangles (3 vertex indices)
		public ObjQuad[] quads;                 // quads (4 vertex indices)
		int vertexBufferId;                     // vertex buffer
		int triangleBufferId;                   // triangle buffer
		int quadBufferId;                       // quad buffer

        // testing normalmaps
        ObjTangent[] tangents;
        int tangentBufferId;

		// constructor
		public Mesh( string fileName )
		{
			MeshLoader loader = new MeshLoader();
			loader.Load( this, "../../assets/" + fileName + ".obj");
		}

		// initialization; called during first render
		public void Prepare( Shader shader )
		{
			if( vertexBufferId == 0 )
			{
				// generate interleaved vertex data (uv/normal/position (total 8 floats) per vertex)
				GL.GenBuffers( 1, out vertexBufferId );
				GL.BindBuffer( BufferTarget.ArrayBuffer, vertexBufferId );
				GL.BufferData( BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * Marshal.SizeOf( typeof( ObjVertex ) )), vertices, BufferUsageHint.StaticDraw );

				// generate triangle index array
				GL.GenBuffers( 1, out triangleBufferId );
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, triangleBufferId );
				GL.BufferData( BufferTarget.ElementArrayBuffer, (IntPtr)(triangles.Length * Marshal.SizeOf( typeof( ObjTriangle ) )), triangles, BufferUsageHint.StaticDraw );
                
                // generate quad index array
                GL.GenBuffers( 1, out quadBufferId );
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, quadBufferId );
				GL.BufferData( BufferTarget.ElementArrayBuffer, (IntPtr)(quads.Length * Marshal.SizeOf( typeof( ObjQuad ) )), quads, BufferUsageHint.StaticDraw );

                // generate tangent space
                tangents = new ObjTangent[triangles.Length];
                CalculateTangentSpace();

                GL.GenBuffers(1, out tangentBufferId);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, tangentBufferId);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(tangents.Length * Marshal.SizeOf(typeof(ObjTangent))), tangents, BufferUsageHint.StaticDraw);
             }
        }

        public void CalculateTangentSpace()
        {
            for (int i = 0; i < triangles.Length; i++)
            {
                int i0 = triangles[i].Index0;
                int i1 = triangles[i].Index1;
                int i2 = triangles[i].Index2;

                Vector3 edge1 = vertices[i1].Vertex - vertices[i0].Vertex;
                Vector3 edge2 = vertices[i2].Vertex - vertices[i0].Vertex;

                float deltaU1 = vertices[i1].TexCoord.X - vertices[i0].TexCoord.X;
                float deltaV1 = vertices[i1].TexCoord.Y - vertices[i0].TexCoord.Y;
                float deltaU2 = vertices[i2].TexCoord.X - vertices[i0].TexCoord.X;
                float deltaV2 = vertices[i2].TexCoord.Y - vertices[i0].TexCoord.Y;

                float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
                Vector3 tangent = Vector3.Zero;
                tangent.X = f * (deltaV2 * edge1.X - deltaV1 * edge2.X);
                tangent.Y = f * (deltaV2 * edge1.Y - deltaV1 * edge2.Y);
                tangent.Z = f * (deltaV2 * edge1.Z - deltaV1 * edge2.Z);

                tangents[i].Vec3 = tangent;
            }
            for (int i = 0; i < tangents.Length; i++)
            {
                tangents[i].Vec3.Normalize();
            }
        }

		// render the mesh using the supplied shader and matrix
		public void Render( Shader shader, Matrix4 transform, Matrix4 toWorld, int texture, Texture normalMap, LightManager lm, Vector3 camera)
        {
			// on first run, prepare buffers
			Prepare( shader );
            
			// safety dance
			GL.PushClientAttrib( ClientAttribMask.ClientVertexArrayBit );

            // enable shader
            GL.UseProgram( shader.programID );

            // enable texture
            GL.Uniform1(shader.uniform_textr, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.BindTexture(TextureTarget.TextureCubeMap, texture);

            //enable normalmap
            if(normalMap != null)
            {
                GL.Uniform1(shader.uniform_nomap, 1);
                GL.ActiveTexture(TextureUnit.Texture1);
                GL.BindTexture(TextureTarget.Texture2D, normalMap.id);
            }

            // pass transform to vertex shader
            GL.UniformMatrix4( shader.uniform_mview, false, ref transform );

            // pass world matrix to vertex shader
            GL.UniformMatrix4(shader.uniform_2wrld, false, ref toWorld);

            // enable lighting         
            float[] arrayLights = lm.GetLights().ToArray();
            GL.Uniform4(shader.uniform_light, arrayLights.Length / 4, arrayLights);

            float[] arrayLightColors = lm.colors.ToArray();
            GL.Uniform3(shader.uniform_lghtc, arrayLightColors.Length / 3, arrayLightColors);

            // use camera location            
            GL.Uniform3(shader.uniform_cmera, camera);

            // enable position, normal and uv attributes
            GL.EnableVertexAttribArray( shader.attribute_vpos );
			GL.EnableVertexAttribArray( shader.attribute_vnrm );
			GL.EnableVertexAttribArray( shader.attribute_vuvs );

			// bind interleaved vertex data
			GL.EnableClientState( ArrayCap.VertexArray );
			GL.BindBuffer( BufferTarget.ArrayBuffer, vertexBufferId );
			GL.InterleavedArrays( InterleavedArrayFormat.T2fN3fV3f, Marshal.SizeOf( typeof( ObjVertex ) ), IntPtr.Zero );

			// link vertex attributes to shader parameters 
			GL.VertexAttribPointer( shader.attribute_vuvs, 2, VertexAttribPointerType.Float, false, 32, 0 );
			GL.VertexAttribPointer( shader.attribute_vnrm, 3, VertexAttribPointerType.Float, true, 32, 2 * 4 );
			GL.VertexAttribPointer( shader.attribute_vpos, 3, VertexAttribPointerType.Float, false, 32, 5 * 4 );

			// bind triangle index data and render
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, triangleBufferId );
			GL.DrawArrays( PrimitiveType.Triangles, 0, triangles.Length * 3 );

			// bind quad index data and render
			if( quads.Length > 0 )
			{
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, quadBufferId );
				GL.DrawArrays( PrimitiveType.Quads, 0, quads.Length * 4 );
			}

			// restore previous OpenGL state
			GL.UseProgram( 0 );
			GL.PopClientAttrib();
		}

		// layout of a single vertex
		[StructLayout( LayoutKind.Sequential )]
		public struct ObjVertex
		{
			public Vector2 TexCoord;
			public Vector3 Normal;
			public Vector3 Vertex;
		}

		// layout of a single triangle
		[StructLayout( LayoutKind.Sequential )]
		public struct ObjTriangle
		{
			public int Index0, Index1, Index2;
		}

        [StructLayout(LayoutKind.Sequential)]
        public struct ObjTangent
        {
            public Vector3 Vec3;
        }

        // layout of a single quad
        [StructLayout( LayoutKind.Sequential )]
		public struct ObjQuad
		{
			public int Index0, Index1, Index2, Index3;
		}
	}
}
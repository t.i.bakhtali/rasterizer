﻿using OpenTK;

namespace Template
{
    class RotatingObject : Node
    {
        float speed;
        public RotatingObject(Mesh _mesh = null, string _shader = null, int _texture = -1, Texture _normalMap = null, float _speed = .001f)
            : base(_mesh, _shader, _texture, _normalMap)
        {
            speed = _speed;
        }

        public override void Animate(float frame)
        {
            base.Animate(frame);
            Rotate(new Vector3(0, 1, 0), speed);
        }
    }
}

using OpenTK;

namespace Template
{
    class AnimatedObject1 : Node
    {
        public AnimatedObject1(Mesh mesh, Texture texture)
            : base(mesh, texture)
        {
        }

        public override void Animate(float frame)
        {
            base.Animate(frame);
            Rotate(new Vector3(1, 1, 1), 0.001f);
        }
    }
}

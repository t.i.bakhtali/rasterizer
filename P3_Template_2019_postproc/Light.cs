using OpenTK;
using System.Collections.Generic;
using System;

namespace Template
{
    public class LightManager
    {
        public List<float> lights;
        public List<float> colors;

        public LightManager()
        {
            lights = new List<float>();
            colors = new List<float>();
        }

        public void AddLight(float[] _pos, float[] _color, float _strength)
        {
            lights.AddRange(_pos);
            lights.Add(_strength);
            colors.AddRange(_color);
        }

        public void AddLight(float _x, float _y, float _z, float _r, float _g, float _b, float _strength)
        {
            lights.Add(_x); lights.Add(_y); lights.Add(_z); lights.Add(_strength);
            colors.Add(_r); colors.Add(_g); colors.Add(_b);
        }

        public List<float> GetLights()
        {
            return lights;
        }

        public List<float> GetTransformedLights()
        {
            return Tlights;
        }

        public void SetTransformedLights(List<float> _Tlights)
        {
            Tlights = _Tlights;
        }
    }
}

﻿using OpenTK;
using System.Collections.Generic;
using System;

namespace Template
{
    public class LightManager
    {
        public List<float> lights;
        public List<float> colors;
        int n;

        public LightManager()
        {
            lights = new List<float>();
            colors = new List<float>();
        }

        public void AddLight(float[] _pos, float[] _color, float _strength)
        {
            lights.AddRange(_pos);
            lights.Add(_strength * 1000);
            colors.AddRange(_color);
            n++;
        }

        public void AddLight(float _x, float _y, float _z, float _r, float _g, float _b, float _strength)
        {
            lights.Add(_x); lights.Add(_y); lights.Add(_z); lights.Add(_strength * 1000);
            colors.Add(_r); colors.Add(_g); colors.Add(_b);
            n++;
        }

        public Vector4 GetLight(int _index)
        {
            if (_index > n - 1)
                return Vector4.Zero;
            return new Vector4(lights[_index], lights[_index++], lights[_index++], lights[_index++]);
        }

        public Vector3 GetLightCol(int _index)
        {
            if (_index > n - 1)
                return Vector3.Zero;
            _index *= 3;
            return new Vector3(colors[_index], colors[_index++], colors[_index++]);
        }

        public List<float> GetLights()
        {
            return lights;
        }

        public void SetLight(int _index, float _x, float _y, float _z)
        {
            if (_index > n - 1)
                return;
            lights[_index] = _x;
            lights[_index + 1] = _y;
            lights[_index + 2] = _z;
        }
    }
}

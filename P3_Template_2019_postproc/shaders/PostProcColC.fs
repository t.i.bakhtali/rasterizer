#version 330

// shader input
in vec2 P;						// fragment position in screen space
in vec2 uv;						// interpolated texture coordinates
uniform sampler2D pixels;		// input texture (1st pass render target)
uniform sampler2D colorcube;
// shader output
out vec3 outputColor;

#define MAXCOLOR 15.0 
#define COLORS 16.0
#define WIDTH 256.0 
#define HEIGHT 16.0

void main()
{
	vec4 original = texture(pixels, uv);
	float cell = original.b * MAXCOLOR;
	float cellL = floor(cell); 
    float cellR = ceil(cell);

	float halfOriginalX = 0.5 / WIDTH; 
    float halfOriginalY = 0.5 / HEIGHT;

	float xOffset = halfOriginalX + original.r / COLORS * (MAXCOLOR / COLORS);
    float yOffset = halfOriginalY + original.g * (MAXCOLOR / COLORS); 

	vec2 cubePosL = vec2(cellL / COLORS + xOffset, yOffset);
	vec2 cubePosR = vec2(cellR / COLORS + xOffset, yOffset);

	vec4 newColorL = texture(colorcube, cubePosL);
	vec4 newColorR = texture(colorcube, cubePosR);

	outputColor = mix(newColorL, newColorR, fract(cell)).xyz;
}
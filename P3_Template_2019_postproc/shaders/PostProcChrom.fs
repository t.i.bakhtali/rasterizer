#version 330

// shader input
in vec2 P;						// fragment position in screen space
in vec2 uv;						// interpolated texture coordinates
uniform sampler2D pixels;		// input texture (1st pass render target)

// shader output
out vec3 outputColor;

void main()
{
	float offset = .001;
    vec4 rValue = texture2D(pixels, P);  
    vec4 gValue = texture2D(pixels, P - offset);
    vec4 bValue = texture2D(pixels, P - 2 * offset);  

    // Combine the offset colors.
    outputColor = vec3(rValue.r, gValue.g, bValue.b);
}

// EOF
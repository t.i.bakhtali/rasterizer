﻿#version 330
 
// shader input
in vec2 vUV;				// vertex uv coordinate
in vec3 vNormal;			// untransformed vertex normal
in vec3 vPosition;			// untransformed vertex position
in vec3 vTangent;

// shader output
out vec4 normal;			// transformed vertex normal
out vec3 worldPos;
out vec2 uv;	
out mat3 TBN;

uniform mat4 transform;
uniform mat4 toWorld;
 
// vertex shader
void main()
{
	// transform vertex using supplied matrix
	gl_Position = transform * vec4(vPosition, 1.0);
	worldPos = (toWorld * vec4(vPosition, 1.0)).xyz;
	// forward normal and uv coordinate; will be interpolated over triangle
	normal = toWorld * vec4( vNormal, 0.0f );
	uv = vUV;

	vec3 T = normalize(vec3(toWorld * vec4(vTangent, 0.0f)));	
	vec3 bitangent = cross(vNormal, vTangent);
	vec3 B = normalize(vec3(toWorld * vec4(bitangent, 0.0f)));
	vec3 N = normalize(vec3(normal));
	TBN = mat3(T, B, N);
	

}
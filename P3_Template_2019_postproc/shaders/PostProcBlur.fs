#version 330

in vec2 P;						
in vec2 uv;		
in vec2 blurTextureCoords[11];

uniform sampler2D pixels;

// shader output
out vec4 outputColor;

float SCurve(float x);

void main()
{
	outputColor = vec4(0.0);
	outputColor += texture(pixels, blurTextureCoords[0]) * 0.0093;
    outputColor += texture(pixels, blurTextureCoords[1]) * 0.028002;
    outputColor += texture(pixels, blurTextureCoords[2]) * 0.065984;
    outputColor += texture(pixels, blurTextureCoords[3]) * 0.121703;
    outputColor += texture(pixels, blurTextureCoords[4]) * 0.175713;
    outputColor += texture(pixels, blurTextureCoords[5]) * 0.198596;
    outputColor += texture(pixels, blurTextureCoords[6]) * 0.175713;
    outputColor += texture(pixels, blurTextureCoords[7]) * 0.121703;
    outputColor += texture(pixels, blurTextureCoords[8]) * 0.065984;
    outputColor += texture(pixels, blurTextureCoords[9]) * 0.028002;
    outputColor += texture(pixels, blurTextureCoords[10]) * 0.0093;
}

float SCurve(float x){
	x = x * 2.0 - 1.0;
	return -x * abs(x) * 0.5 + x + 0.5;
}

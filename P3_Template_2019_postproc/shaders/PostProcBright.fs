#version 330

in vec2 P;						
in vec2 uv;		
in vec4 brightColor;

uniform sampler2D pixels;

// shader output
out vec4 outputColor;

void main()
{

	vec4 color = texture( pixels, uv ); 
	float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 0.5)
		outputColor = vec4(color.rgb, 1.0);
	else
		outputColor = vec4(0.0, 0.0, 0.0, 1.0);
}

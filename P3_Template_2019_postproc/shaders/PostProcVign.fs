#version 330

// shader input
in vec2 P;						// fragment position in screen space
in vec2 uv;						// interpolated texture coordinates
uniform sampler2D pixels;		// input texture (1st pass render target)
uniform vec2 resolution;

// shader output
out vec3 outputColor;

void main()
{
	vec3 color = texture(pixels, uv).rgb;
	vec2 relPos = gl_FragCoord.xy / resolution - 0.5;
	float vignette = smoothstep(1, .4, length(relPos));
	color.rgb = mix(color.rgb, color.rgb * vignette, .8);

	outputColor = color;
}

// EOF
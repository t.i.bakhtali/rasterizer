#version 330

//https://learnopengl.com/Advanced-Lighting/Bloom

in vec2 P;						
in vec2 uv;		
in vec4 brightColor;

uniform sampler2D pixels;
uniform sampler2D brights;

// shader output
out vec4 outputColor;

void main()
{
	const float gamma = 2.2;
	vec3 hdrColor = texture(pixels, uv).rgb;
	vec3 bloomColor = texture(brights, uv).rgb;
	hdrColor += bloomColor;

	outputColor = vec4(hdrColor, 1.0);
}

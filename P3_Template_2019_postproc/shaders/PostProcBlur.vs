#version 330

//https://www.youtube.com/watch?v=uZlwbWqQKpc

in vec2 vUV;				
in vec3 vPosition;		

uniform vec2 resolution;	
uniform int horizontal;
uniform int kernelSize;

out vec2 uv;				
out vec2 P;					
out vec2 blurTextureCoords[11];


// vertex shader
void main()
{
	gl_Position = vec4( vPosition, 1 );
	uv = vUV;
	P = vec2( vPosition ) * 0.5 + vec2( 0.5, 0.5 );
	float pixelSize = 1.0 / kernelSize;

	for(int i = - 5; i <= 5; i++){
		if(horizontal > 0)
			blurTextureCoords[i+5] = P + vec2(pixelSize * i, 0.0);
		else
			blurTextureCoords[i+5] = P + vec2(0.0, pixelSize * i);
	}
	
}

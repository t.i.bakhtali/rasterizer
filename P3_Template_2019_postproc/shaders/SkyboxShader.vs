#version 330

// shader input
in vec3 vPosition;			// untransformed vertex position

// shader output
out vec3 pos;

uniform mat4 transform;

// vertex shader
void main()
{
	pos = vPosition;
	gl_Position = transform * vec4( vPosition, 1 );
}

﻿#version 330
#define NUM_LIGHTS 5
 
// shader input
in vec2 uv;										// interpolated texture coordinates
in vec4 normal;									// interpolated normal
in vec3 worldPos;
in mat3 TBN;

uniform sampler2D pixels;						// texture sampler
uniform sampler2D normalMap;					// normal sampler
uniform vec4 lights[NUM_LIGHTS];				// list of lights
uniform vec3 lColors[NUM_LIGHTS];				// list of light colors
uniform vec3 camera;

// shader output
out vec4 outputColor;

// vars
vec3 n;

// methods
float CalcDiffuse(vec3 lightDir);
float CalcSpecular(vec3 lightDir);

void main()
{
	n = texture(normalMap, uv).rgb;
	n = normalize(n.xyz * 2.0 - 1.0);
	//n = normalize(n * TBN);
	float ambient = 0.3;

	vec3 totalColor = vec3(0, 0, 0);

	outputColor = vec4(totalColor, 1);

	for(int i = 0; i < NUM_LIGHTS; i++)
	{
		vec3 light = lights[i].xyz;
		float strength = lights[i].w;
		vec3 L = light - worldPos;
		float dist = length(L);
		vec3 lightDir = L / dist * TBN;
		float attenuation = 1.0f / (dist * dist);
		totalColor += (CalcDiffuse(lightDir) + CalcSpecular(lightDir)) * strength * lColors[i] * attenuation;
	}

	outputColor = texture( pixels, uv ) * vec4(totalColor, 1);
}

float CalcDiffuse(vec3 lightDir)
{
	return dot(n, lightDir);
}

float CalcSpecular(vec3 lightDir)
{
	float strength = 0.5f;
	vec3 view = normalize(camera - worldPos);

	vec3 reflect = normalize(lightDir - 2.0 * n * dot(n, lightDir)); 
	
	float spec = pow(max(dot(view, reflect), 0.0), 32);
	return strength * spec;
}
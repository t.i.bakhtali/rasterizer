﻿using System;
using System.Collections.Generic;

namespace Template
{
    class ShaderManager
    {
        IDictionary<string, Shader> shaders;

        public ShaderManager()
        {
            shaders = new Dictionary<string, Shader>();
        }

        public void AddShader(string key, Shader shader){
            shaders.Add(key, shader);
        }

        public Shader GetShader(string name)
        {
            if (shaders.ContainsKey(name))
            {
                return shaders[name];
            }
            return null;
        }
    }
}

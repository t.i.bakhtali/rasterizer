﻿using System;
using System.IO;
using OpenTK.Graphics.OpenGL;

namespace Template
{
    public class Shader
    {
        // data members
        public int programID, vsID, fsID;
        public int attribute_vpos;
        public int attribute_vnrm;
        public int attribute_vuvs;
        public int uniform_mview;
        public int uniform_light;
        public int uniform_lghtc;
        public int uniform_2wrld;
        public int uniform_cmera;
        public int uniform_textr;
        public int uniform_scres;
        public int uniform_ccube;

        public int uniform_nomap;
        public int attribute_tang;
        public int attribute_btan;

        public int uniform_hori;
        public int uniform_ksize;

        // constructor
        public Shader(String vertexShader, String fragmentShader)
        {
            PrepShader(vertexShader, fragmentShader);
        }

        // constructor
        public Shader(String name)
        {
            PrepShader(name, name);
        }

        void PrepShader(string vs, string fs)
        {
            programID = GL.CreateProgram();
            Load("../../shaders/" + vs + ".vs", ShaderType.VertexShader, programID, out vsID);

            Load("../../shaders/" + fs + ".fs", ShaderType.FragmentShader, programID, out fsID);
            GL.LinkProgram(programID);
            Console.WriteLine(GL.GetProgramInfoLog(programID));

            // get locations of shader parameters
            attribute_vpos = GL.GetAttribLocation(programID, "vPosition");
            attribute_vnrm = GL.GetAttribLocation(programID, "vNormal");
            attribute_vuvs = GL.GetAttribLocation(programID, "vUV");

            uniform_mview = GL.GetUniformLocation(programID, "transform");
            uniform_light = GL.GetUniformLocation(programID, "lights");
            uniform_lghtc = GL.GetUniformLocation(programID, "lColors");
            uniform_2wrld = GL.GetUniformLocation(programID, "toWorld");
            uniform_cmera = GL.GetUniformLocation(programID, "camera");
            uniform_textr = GL.GetUniformLocation(programID, "pixels");
            uniform_scres = GL.GetUniformLocation(programID, "resolution");

            uniform_nomap = GL.GetUniformLocation(programID, "normalMap");
            attribute_tang = GL.GetAttribLocation(programID, "vTangent");
            attribute_btan = GL.GetAttribLocation(programID, "vBitangent");

            uniform_hori = GL.GetUniformLocation(programID, "horizontal");
            uniform_ksize = GL.GetUniformLocation(programID, "kernelSize");
            uniform_ccube = GL.GetUniformLocation(programID, "colorcube");
        }

        // loading shaders
        void Load(String filename, ShaderType type, int program, out int ID)
        {
            // source: http://neokabuto.blogspot.nl/2013/03/opentk-tutorial-2-drawing-triangle.html
            ID = GL.CreateShader(type);
            using (StreamReader sr = new StreamReader(filename)) GL.ShaderSource(ID, sr.ReadToEnd());
            GL.CompileShader(ID);
            GL.AttachShader(program, ID);
            Console.WriteLine(GL.GetShaderInfoLog(ID));
        }
    }
}

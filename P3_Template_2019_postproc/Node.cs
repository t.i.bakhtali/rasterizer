using System.Collections.Generic;
using System;
using OpenTK;

namespace Template
{
    class Node
    {
        Mesh mesh;
        Texture texture;
        Matrix4 TNode;

        Matrix4 TScale;
        Matrix4 TRotate;
        Matrix4 TTranslate;

        IList<Node> children;

        public Node(Mesh _mesh = null, Texture _texture = null)
        {
            mesh = _mesh;
            texture = _texture;
            children = new List<Node>();
        }

        public void SetTNode(float scale, Vector3 translation, Vector3 axis, float angle)
        {
            TScale = Matrix4.CreateScale(scale);
            TRotate = Matrix4.CreateFromAxisAngle(axis, angle);
            TTranslate = Matrix4.CreateTranslation(translation);
            TNode = TScale * TRotate * TTranslate;
        }

        public void Scale(float scale)
        {
            TScale *= Matrix4.CreateScale(scale);
            UpdateTNode();
        }   

        public void Rotate(Vector3 axis, float angle)
        {
            TRotate *= Matrix4.CreateFromAxisAngle(axis, angle);
            UpdateTNode();
        }

        public void Translate(Vector3 translation)
        {
            TTranslate *= Matrix4.CreateTranslation(translation);
            UpdateTNode();
        }

        public void UpdateTNode()
        {
            TNode = TScale * TRotate * TTranslate;
        }

        public void AddChild(Node node)
        {
            children.Add(node);
        }

        public void Render(Shader shader, Matrix4 parentTransform, float frame)
        {
            Animate(frame);
            Matrix4 transform = TNode * parentTransform;
            if(mesh != null && texture != null)
            {
                mesh.Render(shader, transform, texture);
            }

            foreach(Node child in children)
            {
                child.Render(shader, transform, frame);
            }
        }

        public virtual void Animate(float frame)
        {
        }
    }
}

﻿using System;
using OpenTK;
using OpenTK.Input;

namespace Template
{
    class Camera
    {
        Vector3 axis = new Vector3(0, 0, 1);
        
        public Matrix4 TView;
        public Matrix4 transform;

        MouseState mouse, prevMouse;
        int zoomLevel, zoomMin, zoomMax;

        public Camera(Vector3 offset, float rotation, Matrix4 _TView)
        {
            TView = _TView;
            Matrix4 TRotate = Matrix4.CreateFromAxisAngle(new Vector3(1, 0, 0), -1 * (float)Math.PI / 2);
            Matrix4 TTranslate = Matrix4.CreateTranslation(new Vector3(0, 0, 80));
            transform = TTranslate * TRotate;
            zoomMin = -1000; zoomMax = 1000;
        }

        public Matrix4 GetTransform
        {
            get { return transform.Inverted() * TView; }
        }

        public Matrix3 GetRotation
        {
            get { return (new Matrix3(transform.Column0.Xyz, transform.Column1.Xyz, transform.Column2.Xyz)).Inverted(); }
        }

        public void Rotate(float angle, Vector3 axis)
        {
            axis *= GetRotation;
            transform *= Matrix4.CreateFromAxisAngle(axis, angle);
        }

        public void Translate(Vector3 translation)
        {
            translation *= GetRotation;
            transform *= Matrix4.CreateTranslation(translation);
        }

        public void Update()
        {
            prevMouse = mouse;
            mouse = Mouse.GetState();
            KeyboardState keyboard = Keyboard.GetState();
            if (mouse.LeftButton == ButtonState.Pressed)
            {
                Translate(new Vector3(prevMouse.X - mouse.X, -1 * (prevMouse.Y - mouse.Y), 0) / Map(zoomLevel, zoomMin, zoomMax, 25, 10));
            }
            zoomLevel += (prevMouse.ScrollWheelValue - mouse.ScrollWheelValue) * 4;
            if(zoomLevel < zoomMin || zoomLevel > zoomMax)
            {
                zoomLevel = MathHelper.Clamp(zoomLevel, zoomMin, zoomMax);
            }
            else
            {
                Translate(new Vector3(0, 0, prevMouse.ScrollWheelValue - mouse.ScrollWheelValue));
            }
            if (keyboard.IsKeyDown(Key.Right))
            {
                Rotate(0.01f, new Vector3(0, 1, 0));
            }
            if (keyboard.IsKeyDown(Key.Left))
            {
                Rotate(-0.01f, new Vector3(0, 1, 0));
            }
            if (keyboard.IsKeyDown(Key.Up))
            {
                Rotate(-0.01f, new Vector3(0, 0, 1));
            }
            if (keyboard.IsKeyDown(Key.Down))
            {
                Rotate(0.01f, new Vector3(0, 0, 1));
            }
        }

        public float Map(float x, float in_min, float in_max, float out_min, float out_max)
        {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
    }
}

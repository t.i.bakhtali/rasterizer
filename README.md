# Rasterizer

The features included in this engine are:
- Interactive camera: the camera can be moved and rotated
- A scene graph with unlimited depth
- Phong shading model implemented
- Multiple lights: the amount can be easily changed at run-time, and the lights can be moved realtime
- Normal mapping
- Skybox
- Vignetting and chromatic aberration
- Generic colorization with a color cube
- Box filter with variable kernel width
- HDR glow (or bloom)
